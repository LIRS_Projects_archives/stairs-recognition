###Requirements
********
- Ubuntu 20.04
- ROS Noetic
- Gazebo
- OpenCV 4 (C++)

Clone this project, build workspace. Move my_robot/ to src.
Put absolute path to mesh-file of stair model (src/my_robot/my_robot_gazebo/worlds/stairs.dae) to src/my_robot/my_robot_gazebo/worlds/my_world.world (105)

Do `catkin_make`

Launch world
`$ roslaunch src/my_robot/my_robot_gazebo/launch/my_world.launch`

Launch scripts
`rosrun my_robot_gazebo image_converter`
`rosrun my_robot_gazebo stair_width`
`rosrun my_robot_gazebo step_width`
